//
//  AppDelegate.h
//  Sierpinski
//
//  Created by Philip Huffman on 2015-06-24.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) long requstedDepth;

@end

