//
//  Canvas.m
//  Sierpinski
//
//  Created by Philip Huffman on 2015-06-24.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//
#import "Canvas.h"

static double const SIERPINSKI_ASPECT_RATIO = 0.86602540378444;

@implementation Canvas

/* recieves 1 rect
 * returns self, may be nil
 * a place to overide the init method
 */
- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
    }
    return self;
}

/* recieves sender (unused)
 * request repainting when order altered
 */
- (IBAction)orderDidChange:(id)sender {
    [self setNeedsDisplay];
}

/* UIView method override
 * recieves 1 rect
 * sets canvas location & size
 */
- (void)drawRect:(CGRect)rect {
    [self sierpinski :CGRectMake(rect.origin.x + 32.0,
                                 rect.origin.y + 32.0,
                                 rect.size.width - 32.0,
                                 (rect.size.width - 32.0) * SIERPINSKI_ASPECT_RATIO)];
}

/* recieves 1 rect
 * draws the first triangle and calls the recursive function
 */
-(void) sierpinski :(CGRect)rect {
    self.triangleCounter = 0;
    CGPoint corner1 = CGPointMake(rect.size.width * 0.5, rect.origin.y);
    CGPoint corner2 = CGPointMake(rect.size.width, rect.size.height);
    CGPoint corner3 = CGPointMake(rect.origin.x, rect.size.height);
    
    [self drawTriangle:corner1
                    p1:corner2
                    p2:corner3];
    
    if (0 < self.depthSelector.selectedSegmentIndex) {
        [self subTriangle:1
                       p0:[self findMidpoint:corner1 p1:corner2]
                       p1:[self findMidpoint:corner2 p1:corner3]
                       p2:[self findMidpoint:corner3 p1:corner1]];
    }
//    NSLog(@"Drew %d triangles.", self.triangleCounter);
}

/* recieves 1 long (order) and 3 points
 * recursive function
 * calls itself 3 times if order is not exceded
 */
-(void) subTriangle :(long)n
                  p0:(CGPoint)corner1
                  p1:(CGPoint)corner2
                  p2:(CGPoint)corner3 {

    [self drawTriangle:corner1 p1:corner2 p2:corner3];
    
    if (n < self.depthSelector.selectedSegmentIndex) {
        [self subTriangle:n+1
                       p0:[self findFirstCorner:corner1 p1:corner2 p2:corner3]
                       p1:[self findSecondCorner:corner1 p1:corner2 p2:corner3]
                       p2:[self findMidpoint:corner1 p1:corner2]];

        [self subTriangle:n+1
                       p0:[self findFirstCorner:corner3 p1:corner2 p2:corner1]
                       p1:[self findSecondCorner:corner3 p1:corner2 p2:corner1]
                       p2:[self findMidpoint:corner2 p1:corner3]];

        [self subTriangle:n+1
                       p0:[self findFirstCorner:corner1 p1:corner3 p2:corner2]
                       p1:[self findSecondCorner:corner1 p1:corner3 p2:corner2]
                       p2:[self findMidpoint:corner1 p1:corner3]];
    }
}

/* recieves 3 points
 * returns 1 point (vertex of a subtriangle)
 */
-(CGPoint)findFirstCorner :(CGPoint)corner1 p1:(CGPoint)corner2 p2:(CGPoint)corner3 {
    return CGPointMake((corner1.x + corner2.x) * 0.5 + (corner2.x - corner3.x) * 0.5,
                       (corner1.y + corner2.y) * 0.5 + (corner2.y - corner3.y) * 0.5);
}

/* recieves 3 points
 * returns 1 point (vertex of a subtriangle)
 */
-(CGPoint)findSecondCorner :(CGPoint)corner1 p1:(CGPoint)corner2 p2:(CGPoint)corner3 {
    return CGPointMake((corner1.x + corner2.x) * 0.5 + (corner1.x - corner3.x) * 0.5,
                       (corner1.y + corner2.y) * 0.5 + (corner1.y - corner3.y) * 0.5);
}

/* recieves 2 points
 * returns 1 point
 */
-(CGPoint)findMidpoint :(CGPoint)p0 p1:(CGPoint)p1 {
    return CGPointMake((p0.x + p1.x) * 0.5,
                       (p0.y + p1.y) * 0.5);
}

/* recieves 3 points
 * draws a triangle where the given points are verticies
 */
-(void)drawTriangle:(CGPoint)corner1
                 p1:(CGPoint)corner2
                 p2:(CGPoint)corner3 {
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, nil, corner1.x, corner1.y);
    CGPathAddLineToPoint(path, nil, corner2.x, corner2.y);
    CGPathAddLineToPoint(path, nil, corner3.x, corner3.y);
    CGPathAddLineToPoint(path, nil, corner1.x, corner1.y);

    // add the path to the graphics context
    CGContextRef graphicsContext = UIGraphicsGetCurrentContext();
    CGContextAddPath(graphicsContext, path);
    
    //set a random color for this triangle
    [[self rgb2UIColorWithRed:arc4random_uniform(256)
                        green:arc4random_uniform(256)
                         blue:arc4random_uniform(256)
                        alpha:255] set];
    
    //draw the triangle
    CGContextDrawPath(graphicsContext, kCGPathStroke);
    CGPathRelease(path);
    
    self.triangleCounter++;
}

/* recieves 4 unsigned ints
 * returns 1 UIColor object
 */
-(UIColor *) rgb2UIColorWithRed:(unsigned)r
                          green:(unsigned)g
                           blue:(unsigned)b
                          alpha:(unsigned)a {
    
    return [UIColor colorWithRed:[self hex2float:r]
                           green:[self hex2float:g]
                            blue:[self hex2float:b]
                           alpha:[self hex2float:a]];
}

/* recieves 1 unsigned ints
 * returns 1 double -> min(n / 255, 1)
 */
-(double) hex2float :(unsigned)hex {
    return  hex < 256 ? (double)hex / 255.0 : 1.0;
}

@end
