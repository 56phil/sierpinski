//
//  Canvas.h
//  Sierpinski
//
//  Created by Philip Huffman on 2015-06-24.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface Canvas : UIView
@property (weak, nonatomic) IBOutlet UISegmentedControl *depthSelector;
@property (nonatomic) int triangleCounter;
@end
